(function() {
	'use strict';
	
	function DCSimpleHorizontalBarChartController($scope, $timeout, $q) {
		$scope.DECISYON.target.registerDataConnector(function(requestor) {
		
			var defered = $q.defer();
			$timeout(function() {
			
				defered.resolve(
					[
						{
							chartData		: [88, 7, 54, 33, 12, 75, 99],
							legendLabels	: ['Alpha', 'Beta', 'Charlie', 'Delta', 'Echo', 'Fanta', 'Gamma', 'Hotel', 'India', 'Jakarta', 'King', 'Left']
						}
					]
				);
				
			}, 3000);
			return {
				data: defered.promise
			};
		});
	}
	
	DCSimpleHorizontalBarChartController.$inject = ['$scope', '$timeout', '$q'];
	DECISYON.ng.register.controller('dcSimpleHorizontalBarChartCtrl', DCSimpleHorizontalBarChartController);

}());