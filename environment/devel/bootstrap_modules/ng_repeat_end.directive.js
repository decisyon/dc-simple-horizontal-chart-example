/**
 * #### Decisyon Srl ####
 * Author 	: Pagano Francesco
 * Date 	: 21.08.2015 
 */

(function() {
	'use strict';

	/**
	 * Si occupa capire quando l'NgRepeat su cui � richiamato ha terminato il suo ciclo.
	 */
	function DcyNgRepeatEnd($timeout) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                $timeout(function(){
	                    scope.$emit('dcy.ngRepeat.end');
	                });
	            }
	        }
	    };
	}

	DcyNgRepeatEnd.$inject = ['$timeout'];
	
	angular.module('dcyApp.directives').directive('dcyNgRepeatEnd', DcyNgRepeatEnd);

}());
