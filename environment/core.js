/**
 * Returns if the event was triggered on dashboard page 
 * @param settings
 */
function isOnDshPage(){
	return false;
};

/************************************************************
 * Function to find the root page
 ************************************************************/
function getRootPage(win){
	if (win.location.href.indexOf('home.jsp')!=-1 || win.location.href.indexOf('login.jsp')!=-1){
		return win;
	}
	
	var winParent = win;
	if(win.opener && !win.opener.closed) {
		winParent = getRootPage(win.opener);
	}else if (win.parent && (win.parent.location != win.location)){
		winParent = getRootPage(win.parent);
	}	
	return winParent;
};

/************************************************************
 * Main settings page 
 ************************************************************/	
	var rootPage = null;
	rootPage = getRootPage(window);
	var isOnDshPage = isOnDshPage();
/*********************************************************/

/**
 * Printing message on console by fnToCall
 * @param fnToCall, msg
 */
function printConsoleMsg(fnToCall, e){
	try{
		if (ENV_VARS.isDebugModeEnabled()){
			if (typeof e == "string"){
				fnToCall.apply(console, [e]);
			}
			else{
				fnToCall.apply(console, [e.message]);
			}
		}
	}catch (e) {}
}

/**
 * logToConsole
 * @param msg
 */
function logToConsole(e){
	printConsoleMsg(console.log, e);
};
/**
 * debugToConsole
 * @param msg
 */
function debugToConsole(e){
	printConsoleMsg(console.debug, e);
};
/**
 * errorToConsole
 * @param msg
 */
function errorToConsole(e){
	printConsoleMsg(console.error, e);
};
/**
 * infoToConsole
 * @param msg
 */
function infoToConsole(e){
	printConsoleMsg(console.info, e);
};
